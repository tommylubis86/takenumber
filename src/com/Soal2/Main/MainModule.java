package com.Soal2.Main;

import java.io.*;
import java.util.Scanner;
import java.util.*;
import java.util.List;

import java.lang.Object;
import java.lang.String;
import java.lang.*;
import java.math.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainModule {

	public static void main(String[] args) throws IOException {

		boolean isNumber = false;
		boolean isInRange = false;

		boolean isEvenNumber = false;
		
		do {
		    System.out.print("input number (even) between 10 and 20 ==> ");
		    int InputUser = ReadInt();
		
			try {
	
				if ((InputUser >= 10) && (InputUser <= 20)) {
					isNumber = true;
					isInRange = true;
					if(InputUser % 2 ==0) { isEvenNumber = true; }
					 else { isEvenNumber = false; }
				} else {
					isNumber = true;
					isInRange = false;
				}
	
			} catch (Exception exc1) {
				System.err.println("Error, Input Must be number between 10 and 20!!!");
				isNumber = false;
				isInRange = false;
			}

			if (isNumber) {
				if (isInRange) {
					if(isEvenNumber) {
						letsPlayNumberInput(InputUser); 
					} else 
					{
						System.out.println();
						System.out.println("Please input even number, don't input odd number .... ");
						System.out.println();	
					}
				}
			}
	  
		 } while(isEvenNumber==false);
	}

	private static void letsPlayNumberInput(int inputUser) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		int userInput;
		boolean isFinished = false;
		boolean isNumeric = false;
		boolean isCorrectPosition = false;

		int totalItemInList = inputUser;

		int newTotalItemInList = 0;

		int userSelectedNumber = 0;
		int programAutoSelectNumber = 0;
		int sumOfUserNumber = 0;
		int sumOfSystemNumber = 0;

		String endingStatus = "";

		int listNumber[] = null;

		listNumber = new int[totalItemInList];

		for (int i = 0; i < totalItemInList; i++) {
			listNumber[i] = getRandomNumberInRange(10, 20); // take randomize number into each element array
		}

		do {

			System.out.print("List ==>  ");
			for (int x = 0; x < totalItemInList; x++) {
				System.out.print(" " + listNumber[x]);
			}
			System.out.println();
			System.out.print("choose a number in most-right or most-left position : ");

			userInput = ReadInt();

			try {
				userSelectedNumber = userInput;
				isNumeric = true;
			} catch (Exception excParse) {
				System.out.println("Error while parsing input --> " + excParse.getMessage());
				isNumeric = false;
			}

			if (isNumeric == true) {

				if (userSelectedNumber == listNumber[0]) {					
					isCorrectPosition = true;
					programAutoSelectNumber = listNumber[1];

					newTotalItemInList = totalItemInList - 2;

					int tempArray[] = new int[newTotalItemInList];
					for (int i = 2; i < totalItemInList; i++) {
						int currentIndex = i - 2;
						tempArray[currentIndex] = listNumber[i];
					}
					// rebuild
					listNumber = new int[newTotalItemInList];
					for (int x = 0; x < newTotalItemInList; x++) {
						listNumber[x] = tempArray[x];
					}
					totalItemInList = newTotalItemInList;

				} else {
					int lastIndex = totalItemInList - 1;
					if (userSelectedNumber == listNumber[lastIndex]) {
						isCorrectPosition = true;
						int beforeLastIndex = lastIndex - 1;
						programAutoSelectNumber = listNumber[beforeLastIndex];

						newTotalItemInList = totalItemInList - 2;

						int tempArray2[] = new int[newTotalItemInList];
						for (int i = 0; i < newTotalItemInList; i++) {
							tempArray2[i] = listNumber[i];
						}
						// rebuild
						listNumber = new int[newTotalItemInList];
						for (int x = 0; x < newTotalItemInList; x++) {
							listNumber[x] = tempArray2[x];
						}
						totalItemInList = newTotalItemInList;

					} else {
						System.out.println("Please pick the most left  or most right position");
						System.out.println();
						isCorrectPosition = false;
					}
				}

				if (isCorrectPosition == true) {

					sumOfUserNumber = sumOfUserNumber + userSelectedNumber;
					sumOfSystemNumber = sumOfSystemNumber + programAutoSelectNumber;

					System.out.println("You Pick : " + userSelectedNumber);
					System.out.println("AI Pick  : " + programAutoSelectNumber);
					System.out.println("Sum User ==> " + sumOfUserNumber);
					System.out.println("Sum AI ==> " + sumOfSystemNumber);
					System.out.println();

					if (totalItemInList == 0) {
						isFinished = true;
						if (sumOfSystemNumber > sumOfUserNumber) {
							endingStatus = "Game Over, You Lost !!";
						} else {
							if (sumOfSystemNumber < sumOfUserNumber) {
								endingStatus = "Finished, Congratulation, You Win !!";
							} else {
								endingStatus = "Finished, Draw";
							}
						}
					}

				} // end of block if (isCorrectPosition == true)

			} else {
				System.out.println("You Must Input number (integer), choose one from most right or most left");
				isFinished = false;
			}


		} while (!isFinished);
		
		System.out.println();
		System.out.println(endingStatus);

	}

	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	private static int ReadInt() throws java.io.IOException {
		String str = "";
		char c;
		while (true) {
			c = (char) System.in.read();
			if ((c == '\n') || (c == ' ')) {
				break;
			}
			str += c;
		}
		Integer n = Integer.parseInt(str.trim());
		return n.intValue();
	}

}
